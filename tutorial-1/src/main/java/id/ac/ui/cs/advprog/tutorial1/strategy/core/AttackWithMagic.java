package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me
    public AttackWithMagic(){}

    @Override
    public String attack() {
        return "Sihir";
    }

    @Override
    public String getType() {
        return "Magic";
    }
}
