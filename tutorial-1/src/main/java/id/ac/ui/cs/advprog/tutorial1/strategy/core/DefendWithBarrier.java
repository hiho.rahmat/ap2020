package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    public DefendWithBarrier(){}

    @Override
    public String defend() {
        return "Barrier";
    }

    @Override
    public String getType() {
        return "Barrier";
    }
    //ToDo: Complete me
}
