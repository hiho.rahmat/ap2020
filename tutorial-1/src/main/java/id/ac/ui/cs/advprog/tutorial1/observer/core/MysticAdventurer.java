package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                //ToDo: Complete Me
                this.guild = guild;
                this.guild.add(this);
        }

        @Override
        public void update() {
                String qType = guild.getQuestType();
                if(qType.compareTo("E")==0 || qType.compareTo("D")==0){
                        addQuest(guild.getQuest());
                }
        }

        //ToDo: Complete Me
}
